//
//  MainViewModelTests.swift
//  TestTests
//
//  Created by Robert on 15-02-18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//

import XCTest

@testable import Test
class MainViewModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetCounterFromViewModel() -> Void {
        let viewModel = MainViewModel()
        let promise = expectation(description: "Request from VieModel - Get Counter")
        viewModel.getCounter { (data, response, error) in
            
            XCTAssertNotNil(data, "data should not be nil")
            XCTAssertNil(error, "error should be nil")
            
            
            
            if let response = response as? HTTPURLResponse,
                let responseURL = response.url
            {
                XCTAssertEqual(responseURL.absoluteString, "\(baseURL)\(UrlPath.getCounter.rawValue)", "URL es igual a la URL de la petición")
                XCTAssertEqual(response.statusCode, 200, "El estado de la respuesta es 200")
                
                if response.statusCode == 200 {
                    guard let result = data else {
                        XCTFail("Viene Nil")
                        return
                    }
                    
                    XCTAssertTrue(result != [])
                    
                }
                
            } else {
                XCTFail("Error")
            }
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
    }
    
    func testDeleteCounterFromViewModel() -> Void {
        let viewModel = MainViewModel()
        let promise = expectation(description: "Request from VieModel - Delete Counter")
        viewModel.deleteCounter(id: "jdonj17x") { (data, response, error) in
            
            XCTAssertNotNil(data, "data should not be nil")
            XCTAssertNil(error, "error should be nil")
            
            if let response = response as? HTTPURLResponse,
                let responseURL = response.url
            {
                XCTAssertEqual(responseURL.absoluteString, "\(baseURL)\(UrlPath.addCounter.rawValue)", "URL es igual a la URL de la petición")
                XCTAssertEqual(response.statusCode, 200, "El estado de la respuesta es 200")
            
            }
            
            guard let result = data else {
                XCTFail("Viene Nil")
                return
            }
            
            XCTAssertTrue(result.count <= 5 )
            
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
    }
    
    func testUpdateCounterFromViewModel() -> Void {
        let viewModel = MainViewModel()
        let promise = expectation(description: "Request from VieModel - Update Counter")
        // Cambiar el id por el que quiera hacer el update
        viewModel.updateCounter(id: "jdoittdp", isInc: true) { (data, error) in
            
            XCTAssertNotNil(data, "data should not be nil")
            XCTAssertNil(error, "error should be nil")
            
            guard let result = data else {
                XCTFail("Viene Nil")
                return
            }
            
            XCTAssertTrue(result != [])
            let filtered = result.filter { $0.id == "jdoittdp"}
            XCTAssertTrue(filtered.count != 40)
            
            
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
    }
    
}
