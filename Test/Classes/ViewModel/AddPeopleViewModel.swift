//
//  AddPeopleViewModel.swift
//  Test
//
//  Created by Robert on 13-02-18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//

import UIKit

class AddPeopleViewModel: NSObject {
    
    var peopleModel: [Counter]?

    func addCounter(title: String, count: Int, callback: @escaping (_ data: [Counter]?, _ error: NSError?) -> Void) {
        
        peopleModel = [Counter]()
        Counter.addCounter(title: title, count: count, callback: { (data, error) in
            
            if error != nil {
                callback(nil, error)
                
            } else {
                self.peopleModel = data
                
                callback(self.peopleModel, nil)
            }
            
        })
    }
    
}
