//
//  MainViewModel.swift
//  Test
//
//  Created by Robert on 10-02-18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//

import UIKit

class MainViewModel: NSObject {
    
    var peopleModel: [Counter]?
    
    func getCounter(callback: @escaping (_ data: [Counter]?, _ response: URLResponse?, _ error: NSError?) -> Void) {
        
        peopleModel = [Counter]()
        
        Counter.getCounter { [weak self] (data, response, error) in
            if data == nil {
                let newCounters = CounterCoreData.getAllCounter()
                self?.peopleModel = newCounters
                callback(nil, response, error)
                
            } else {
                self?.peopleModel = data
                callback(self?.peopleModel, response, nil)
            }
        }
        
    }
    
    func updateCounter(id: String, isInc: Bool, callback: @escaping (_ data: [Counter]?, _ error: NSError?) -> Void) {
        
        Counter.updateCounter(id: id, isInc: isInc) { [weak self] (data, error) in
            guard let data = data else {
                
                let newCounters = CounterCoreData.getAllCounter()
                
                for obj in newCounters! {
                    
                    if obj.id == id {
                        if isInc {
                            obj.count = obj.count! + 1
                            CounterCoreData.saveCounter(data: obj)
                            
                        } else {
                            obj.count = obj.count! - 1
                            CounterCoreData.saveCounter(data: obj)
                        }
                    }
                    
                }
                
                self?.peopleModel = newCounters
                
                CounterCoreData.saveCounter()
                
                callback(newCounters, error! as NSError)
                return
            }
            
            self?.peopleModel = data
            
            callback(self?.peopleModel, nil)
        }
        
    }
    
    func deleteCounter(id: String, callback: @escaping (_ data: [Counter]?, _ response: URLResponse?, _ error: NSError?) -> Void) {
        
        Counter.deleteCounter(id: id) { [weak self] (data, response, error) in
            guard let data = data else {
                CounterCoreData.deleteCounter(id: id)
                callback(CounterCoreData.getAllCounter(), response, error! as NSError)
                return
            }
            
            self?.peopleModel = data
            
            callback(self?.peopleModel, response, nil)
        }
        
    }

}
