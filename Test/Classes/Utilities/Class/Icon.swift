//
//  Icon.swift
//  Test
//
//  Created by Robert on 12-02-18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//

import UIKit

@IBDesignable
class Icon: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBInspectable var fillColor: UIColor = UIColor.white
    
    private var plusLineWidth: CGFloat = 1.0
    private let plusButtonScale: CGFloat = 0.6
    private let halfPointShift: CGFloat = 0.5
    
    private var halfWidth: CGFloat {
        return bounds.width / 2
    }
    
    private var halfHeight: CGFloat {
        return bounds.height / 2
    }
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        let circle = UIBezierPath(ovalIn: rect)
        fillColor.setFill()
        circle.addClip()
        circle.fill()
        
        let widthBody: CGFloat = 25
        let heightBody: CGFloat = 25
        let body = UIBezierPath(ovalIn: CGRect(x: halfWidth - widthBody/2, y: halfHeight, width: heightBody, height: heightBody))
        UIColor.black.setFill()
        body.fill()
        
        let widthHead: CGFloat = 12
        let heightHead: CGFloat = 12
        
        let path = UIBezierPath(ovalIn: CGRect(x: halfWidth - widthHead/2, y: halfHeight - heightHead + 4, width: widthHead, height: heightHead))
        fillColor.setFill()
        path.fill()
        
        let head = UIBezierPath(ovalIn: CGRect(x: halfWidth - widthHead/2, y: halfHeight - heightHead + 2, width: widthHead, height: heightHead))
        UIColor.black.setFill()
        head.addClip()
        head.fill()
    }

}
