//
//  PlusMinusButton.swift
//  Test
//
//  Created by Robert on 10-02-18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//

import UIKit

@IBDesignable
class PlusMinusButton: UIButton {
    
    @IBInspectable var fillColor: UIColor = UIColor.blue
    @IBInspectable var isPlus: Bool = true
    @IBInspectable var isOnlyFill: Bool = false
    @IBInspectable var isRotate: Bool = false
    
    private var plusLineWidth: CGFloat = 1.0
    private let plusButtonScale: CGFloat = 0.6
    private let halfPointShift: CGFloat = 0.5
    
    private var halfWidth: CGFloat {
        return bounds.width / 2
    }
    
    private var halfHeight: CGFloat {
        return bounds.height / 2
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    */
    
    override func layoutSubviews() {
    }
    
    override func draw(_ rect: CGRect) {
        
        if !isPlus {
            plusLineWidth = 1.2
        } else {
            plusLineWidth = 1.0
        }
        
        // Drawing code
        let path = UIBezierPath(ovalIn: rect)
        fillColor.setFill()
        path.addClip()
        path.fill()
        
        if isOnlyFill != true {
            let plusWidth: CGFloat = min(bounds.width, bounds.height) * plusButtonScale
            let halfPlusWidth = plusWidth / 2
            
            let plusPath = UIBezierPath()
            
            plusPath.lineWidth = plusLineWidth
            
            // Horizontal
            plusPath.move(to: CGPoint(
                x: halfWidth - halfPlusWidth + halfPointShift,
                y: halfHeight + halfPointShift))
            
            plusPath.addLine(to: CGPoint(
                x: halfWidth + halfPlusWidth + halfPointShift,
                y: halfHeight + halfPointShift))
            
            if isPlus {
                // Vertical
                plusPath.move(to: CGPoint(
                    x: halfWidth + halfPointShift,
                    y: halfHeight - halfPlusWidth + halfPointShift))
                
                plusPath.addLine(to: CGPoint(
                    x: halfWidth + halfPointShift,
                    y: halfHeight + halfPlusWidth + halfPointShift))
            }
            
            UIColor.white.setStroke()
            plusPath.stroke()
            
            if isRotate {
                self.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 4)
            } else {
                self.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            }
            
        
        } else {
            
            let widthBody: CGFloat = 25
            let heightBody: CGFloat = 25
            let body = UIBezierPath(ovalIn: CGRect(x: halfWidth - widthBody/2, y: halfHeight, width: heightBody, height: heightBody))
            UIColor.white.setFill()
            body.fill()
            
            let widthHead: CGFloat = 12
            let heightHead: CGFloat = 12
            
            let path = UIBezierPath(ovalIn: CGRect(x: halfWidth - widthHead/2, y: halfHeight - heightHead + 4, width: widthHead, height: heightHead))
            fillColor.setFill()
            path.fill()
            
            let head = UIBezierPath(ovalIn: CGRect(x: halfWidth - widthHead/2, y: halfHeight - heightHead + 2, width: widthHead, height: heightHead))
            UIColor.white.setFill()
            head.addClip()
            head.fill()
            
            
        }
        
        
    }

}
