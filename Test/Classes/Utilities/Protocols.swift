//
//  Protocols.swift
//  Test
//
//  Created by Robert on 12-02-18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//

import Foundation

protocol ItemDelegate {
    func addNewItem() -> Void
    func updateItem(data: Counter, isInc: Bool, index: IndexPath) -> Void
    func deleteItem(data: Counter) -> Void
}

@objc
protocol AddCounterDelegate {
    func insertCounter(data: [Counter]) -> Void
}
