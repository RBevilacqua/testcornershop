//
//  CollectionViewCell.swift
//  Test
//
//  Created by Robert on 10-02-18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var minusButton: PlusMinusButton!
    @IBOutlet weak var plusButton: PlusMinusButton!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var loadingView: SpinnerView!
    @IBOutlet weak var contentSpinnerView: UIView!
    
    var pan: UIPanGestureRecognizer!
    var deleteLabel1: UILabel!
    var deleteLabel2: UILabel!
    var index: IndexPath?
    
    var delegate: ItemDelegate?
    
    var counter: Counter?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if (pan.state == UIGestureRecognizerState.changed) {
            let p: CGPoint = pan.translation(in: self)
            let width = self.contentView.frame.width
            let height = self.contentView.frame.height
            self.contentView.frame = CGRect(x: p.x,y: 0, width: width, height: height)
            self.deleteLabel2.frame = CGRect(x: screenSize.width - 80, y: 0, width: 60, height: height)
        }
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureCell()
    }
    
    
    func configureCell() -> Void {
        
        self.contentView.backgroundColor = .white
        
        self.backgroundColor = .red
        
        deleteLabel2 = UILabel(frame: CGRect(x: screenSize.width - 80, y: self.contentView.frame.height / 2, width: 60, height: self.contentView.frame.height))
        deleteLabel2.text = "delete"
        deleteLabel2.textColor = UIColor.white
        self.insertSubview(deleteLabel2, belowSubview: self.contentView)
        
        pan = UIPanGestureRecognizer(target: self, action: #selector(onPan(_:)))
        pan.delegate = self
        self.addGestureRecognizer(pan)
    }
    
    @IBAction func incrementAction(_ sender: UIButton) {
        self.quantityLabel.text = "\(Int(self.quantityLabel.text!)! + 1)"
        // Protocol
        self.delegate?.updateItem(data: counter!, isInc: true, index: index!)
        
    }
    
    @IBAction func decrementAction(_ sender: UIButton) {
        if Int(self.quantityLabel.text!) == 1 {
            self.quantityLabel.text = "\(Int(self.quantityLabel.text!)! - 1)"
            let collectionView: UICollectionView = self.superview as! UICollectionView
            let indexPath: IndexPath = collectionView.indexPathForItem(at: self.center)!
            collectionView.delegate?.collectionView!(collectionView, performAction: #selector(onPan(_:)), forItemAt: indexPath, withSender: nil)
            self.delegate?.deleteItem(data: counter!)
        
        } else {
            self.quantityLabel.text = "\(Int(self.quantityLabel.text!)! - 1)"
            // Protocol
            self.delegate?.updateItem(data: counter!, isInc: false, index: index!)
            
        }
        
    }
    

}

// Gesture
extension CollectionViewCell: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return abs((pan.velocity(in: pan.view)).x) > abs((pan.velocity(in: pan.view)).y)
    }
    
    @objc func onPan(_ pan: UIPanGestureRecognizer) {
        
        let velocity = pan.velocity(in: self)
        
        if velocity.x > 0 {
            return
        }
        
        if pan.state == UIGestureRecognizerState.began {
            
        } else if pan.state == UIGestureRecognizerState.changed {
            self.setNeedsLayout()
        } else {
            if abs(pan.velocity(in: self).x) > 200 {
                let collectionView: UICollectionView = self.superview as! UICollectionView
                let indexPath: IndexPath = collectionView.indexPathForItem(at: self.center)!
                collectionView.delegate?.collectionView!(collectionView, performAction: #selector(onPan(_:)), forItemAt: indexPath, withSender: nil)
                self.delegate?.deleteItem(data: counter!)
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.setNeedsLayout()
                    self.layoutIfNeeded()
                })
            }
        }
    }
}
