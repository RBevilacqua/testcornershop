//
//  HeaderCollectionViewCell.swift
//  Test
//
//  Created by Robert on 11-02-18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//

import UIKit

class HeaderCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var plusButton: PlusMinusButton!
    
    var delegate: ItemDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        defer {
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        defer {
        }
    }

    @IBAction func addItemAction(_ sender: UIButton) {
        delegate?.addNewItem()
    }
    
}
