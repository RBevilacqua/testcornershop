//
//  FooterCollectionViewCell.swift
//  Test
//
//  Created by Robert on 12-02-18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//

import UIKit

class FooterCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var totalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadTotal), name: NSNotification.Name("reloadTotal"), object: nil)
        
    }
    
    @objc func reloadTotal(notification: NSNotification) -> Void {
        self.totalLabel.text = "\(notification.userInfo!["total"]!)"
    }

}
