//
//  MainViewController.swift
//  Test
//
//  Created by Robert on 10-02-18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var numberRow: Int? = 10
    
    var viewModel: MainViewModel = MainViewModel()
    var loading: Loading?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "Test - Cornershop"
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerCollectionCells(nibName: "CollectionViewCell", cellIdentifier: "CollectionViewCell")
        collectionView.register(UINib(nibName: "HeaderCollectionViewCell", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCollectionViewCell")
        
        collectionView.register(UINib(nibName: "FooterCollectionViewCell", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "FooterCollectionViewCell")
        
        let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.sectionHeadersPinToVisibleBounds = true
        layout?.sectionFootersPinToVisibleBounds = true
        
        loading = Loading()
        loading?.show()

        viewModel.getCounter { [weak self] (data, response, error) in
            
            DispatchQueue.main.async { [weak self] in
                
                self?.loading?.dismiss()
                
                if error != nil {
                    self?.collectionView.reloadData()
                    Message.show(text: "Ha ocurrido un error / Sin Internet. Se ven funciones Limitadas", type: .warning)
                } else {
                    
                    self?.collectionView.reloadData()
                }
            }

            

        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func registerCollectionCells(nibName: String, cellIdentifier: String) -> Void {
        let listNib = UINib(nibName: nibName, bundle: nil)
        self.collectionView.register(listNib, forCellWithReuseIdentifier: cellIdentifier)
    }
    
    @objc func changePosition() -> Void {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        var message = Message()
//        message.show()
        
    }


}


extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.peopleModel?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        cell.quantityLabel.text = "\(viewModel.peopleModel![indexPath.row].count ?? 0)"
        cell.titleLabel.text = viewModel.peopleModel![indexPath.row].title
        cell.counter = viewModel.peopleModel?[indexPath.row]
        cell.index = indexPath
        cell.delegate = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionFooter {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: "FooterCollectionViewCell",
                                                                             for: indexPath) as! FooterCollectionViewCell
            
            var sum = 0
            viewModel.peopleModel!.forEach({ sum += $0.count!})
            footerView.totalLabel.text = "\(sum)"
            
            return footerView
            
        } else {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: "HeaderCollectionViewCell",
                                                                             for: indexPath) as! HeaderCollectionViewCell
            headerView.delegate = self
            return headerView
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: screenSize.width, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: screenSize.width, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: screenSize.width, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        Message.show(text: "\(viewModel.peopleModel![indexPath.row].title ?? "\(indexPath.row)") fue elminado", type: .warning)
        viewModel.peopleModel!.remove(at: indexPath.row)
        collectionView.deleteItems(at: [indexPath])
    }
    
}

// Delegate Header
extension MainViewController: ItemDelegate {
    
    func deleteItem(data: Counter) {
        viewModel.deleteCounter(id: data.id!) { (data, response, error) in
            
            DispatchQueue.main.async {
                
                if error != nil {
                    print("Error")
                    Message.show(text: "Ups!, Ocurrio un error", type: .warning)
                    var sum = 0
                    data?.forEach({ sum += $0.count!})
                    NotificationCenter.default.post(name: Notification.Name("reloadTotal"), object: nil, userInfo: ["total":sum])
                } else {
                    print("success")
                    Message.show(text: "Se ha eliminado satisfactoriamente", type: .success)
                    var sum = 0
                    data?.forEach({ sum += $0.count!})
                    NotificationCenter.default.post(name: Notification.Name("reloadTotal"), object: nil, userInfo: ["total":sum])
                    
                }
            }
            
            
        }
    }
    
    func updateItem(data: Counter, isInc: Bool, index: IndexPath) {
        
        
        let cell = self.collectionView.cellForItem(at: index) as! CollectionViewCell
        cell.minusButton.alpha = 0.5
        cell.plusButton.alpha = 0.5
        cell.quantityLabel.alpha = 0.2
        cell.contentSpinnerView.alpha = 1.0
        
        viewModel.updateCounter(id: data.id!, isInc: isInc) { (data, error) in
            
            DispatchQueue.main.async {
                
                if error != nil {
                    
                    cell.minusButton.alpha = 1.0
                    cell.plusButton.alpha = 1.0
                    cell.quantityLabel.alpha = 1.0
                    cell.contentSpinnerView.alpha = 0.0
                    
                    if data != nil {
                        
                        var sum = 0
                        data?.forEach({ sum += $0.count!})
                        NotificationCenter.default.post(name: Notification.Name("reloadTotal"), object: nil, userInfo: ["total":sum])
                        
                    } else {
                        Message.show(text: "Ups!, ocurrio un error. Intentalo más tarde", type: .warning)
                        
                    }
                    
                } else {
                    
                    cell.minusButton.alpha = 1.0
                    cell.plusButton.alpha = 1.0
                    cell.quantityLabel.alpha = 1.0
                    cell.contentSpinnerView.alpha = 0.0
                    
                    var sum = 0
                    data?.forEach({ sum += $0.count!})
                    NotificationCenter.default.post(name: Notification.Name("reloadTotal"), object: nil, userInfo: ["total":sum])
                    
                }
                
            }
            
        }
    }
    
    
    func addNewItem() {
        let addCounter = UIStoryboard(name: "AddPeople", bundle: nil).instantiateViewController(withIdentifier: "AddPeopleViewController") as! AddPeopleViewController
        
        addCounter.delegate = self
        self.present(addCounter, animated: true)
    }
    
    
}

extension MainViewController: AddCounterDelegate {
    
    func insertCounter(data: [Counter]) {
        viewModel.peopleModel = data
        
        self.collectionView.performBatchUpdates({
            self.collectionView.insertItems(at: [IndexPath(item: ((viewModel.peopleModel?.count)! - 1), section: 0)])
        }, completion: nil)
        
        
    }
    
}
