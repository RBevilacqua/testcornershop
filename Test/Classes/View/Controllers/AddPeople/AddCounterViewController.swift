//
//  AddPeopleViewController.swift
//  Test
//
//  Created by Robert on 13-02-18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//

import UIKit

class AddPeopleViewController: UIViewController {
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var contentAddPeople: UIStackView!
    @IBOutlet weak var addPeopleButton: PlusMinusButton!
    @IBOutlet weak var constraintStackView: NSLayoutConstraint!
    
    var viewModel: AddPeopleViewModel = AddPeopleViewModel()
    
    weak var delegate: AddCounterDelegate?
    
    var loading: Loading?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        saveButton.layer.cornerRadius = 4
        addForm()
        loading = Loading()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addForm() -> Void {
        let view = UINib(nibName: "Add", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! Add
        view.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: 60)
        self.contentAddPeople.addArrangedSubview(view)
        self.constraintStackView.constant = CGFloat(self.contentAddPeople.arrangedSubviews.count * 60)
    }
    
    @IBAction func addPeopleAction(_ sender: UIButton) {
//        addForm()
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionSave(_ sender: UIButton) {
        
//        for index in 0..<self.contentAddPeople.arrangedSubviews.count {
        loading?.show()
        if (self.contentAddPeople.arrangedSubviews.first as! Add).nameTextField.text != "" {
            viewModel.addCounter(title: (self.contentAddPeople.arrangedSubviews.first as! Add).nameTextField.text!, count: Int((self.contentAddPeople.arrangedSubviews.first as! Add).quantityTextField.text!)!) { [weak self] (data, error) in
                
                DispatchQueue.main.async { [weak self] in
                    
                    self?.loading?.dismiss()
                    if error != nil {
                        print("Error")
                        Message.show(text: "Ups!, Ocurrio un error", type: .warning)
                    } else {
                        print("success")
                        Message.show(text: "\((self?.contentAddPeople.arrangedSubviews.first as! Add).nameTextField.text!) fue agregado satisfactoriamente", type: .success, completion: {
                            
                        })
                        self?.delegate?.insertCounter(data: data!)
                        self?.dismiss(animated: true, completion: nil)
                        
                    }
                }
                
                
            }
        }
        
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
