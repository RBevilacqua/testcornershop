//
//  Counter.swift
//  Test
//
//  Created by Robert on 10-02-18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//

import UIKit

class Counter: NSObject, Codable {
    
    var id: String?
    var title: String?
    var count: Int?
    
    // Add Counter to DB
    class func getCounter(callback: @escaping (_ data: [Counter]?, _ response: URLResponse?, _ error: NSError?) -> Void) -> Void {
        
        Services.requestJSON(path: .getCounter, method: .get, params: nil) { (data, response, error) in
            
            if error != nil {
                callback(nil, response, error! as NSError)
                
            } else {
                do {
                    
                    //Decode retrived data with JSONDecoder and assing type of Article object
                    let data = try JSONDecoder().decode([Counter].self, from: data!)
                    
                    //Get back to the main queue
                    DispatchQueue.main.async {
                        print(data)
                        for counter in data {
                            CounterCoreData.saveCounter(data: counter)
                        }
                        
                        callback(data, response, nil)
                    }
                    
                } catch let jsonError {
                    print(jsonError)
                }
                
            }
            
            
        }
        
    }
    
    // Add Counter to DB
    class func addCounter(title: String, count: Int, callback: @escaping (_ data: [Counter]?, _ error: NSError?) -> Void) -> Void {
        
        let params: [String: Any] = [
            "title": title,
            "count": count
        ]
        
        Services.requestJSON(path: .addCounter, method: .post, params: params) { (data, response, error) in
            
            if error != nil {
                callback(nil, error! as NSError)
                
            } else {
                do {
                    //Decode retrived data with JSONDecoder and assing type of Article object
                    let data = try JSONDecoder().decode([Counter].self, from: data!)
                    
                    //Get back to the main queue
                    DispatchQueue.main.async {
                        print(data)
                        for counter in data {
                            CounterCoreData.saveCounter(data: counter)
                        }
                        callback(data, nil)
                    }
                    
                } catch let jsonError {
                    print(jsonError)
                }
                
            }
            
            
        }
        
    }
    
    class func updateCounter(id: String, isInc: Bool, callback: @escaping (_ data: [Counter]?, _ error: NSError?) -> Void) -> Void {
        
        let params: [String: Any] = [
            "id": id
        ]
        
        Services.requestJSON(path: isInc != true ? .decCounter : .incCounter, method: .post, params: params) { (data, response, error) in
            
            if error != nil {
                callback(nil, error! as NSError)
                
            } else {
                do {
                    //Decode retrived data with JSONDecoder and assing type of Article object
                    let data = try JSONDecoder().decode([Counter].self, from: data!)
                    
                    //Get back to the main queue
                    DispatchQueue.main.async {
                        print(data)
                        let filtered = data.filter{ $0.id == id }[0]
                        CounterCoreData.saveCounter(data: filtered)
                        callback(data, nil)
                    }
                    
                } catch let jsonError {
                    print(jsonError)
                }
                
            }
            
            
        }
        
    }
    
    // Delete Counter to DB
    class func deleteCounter(id: String, callback: @escaping (_ data: [Counter]?, _ response: URLResponse?, _ error: NSError?) -> Void) -> Void {
        
        let params: [String: Any] = [
            "id": id
        ]
        
        Services.requestJSON(path: .addCounter, method: .delete, params: params) { (data, response, error) in
            
            if error != nil {
                callback(nil, response, error! as NSError)
                
            } else {
                do {
                    //Decode retrived data with JSONDecoder and assing type of Article object
                    let data = try JSONDecoder().decode([Counter].self, from: data!)
                    
                    //Get back to the main queue
                    DispatchQueue.main.async {
                        print(data)
                        CounterCoreData.deleteCounter(id: id)
                        callback(data, response, nil)
                    }
                    
                } catch let jsonError {
                    print(jsonError)
                }
                
            }
            
            
        }
        
    }

}
