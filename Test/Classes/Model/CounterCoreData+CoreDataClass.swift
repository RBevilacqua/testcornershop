//
//  CounterCoreData+CoreDataClass.swift
//  Test
//
//  Created by Robert on 15-02-18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//
//

import Foundation
import CoreData
import UIKit

@objc(CounterCoreData)
public class CounterCoreData: NSManagedObject {

    class func saveCounter(data: Counter? = nil, id: String? = nil) -> Void {
        
        if data != nil || id != nil {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CounterCoreData")
            request.predicate = NSPredicate(format: "id = %@", data?.id ?? id!)
            request.returnsObjectsAsFaults = false
            
            do {
                let result = try context.fetch(request) as! [CounterCoreData]
                
                if result.count > 0 {
                    for obj in result {
                        obj.count = Int16(data!.count!)
                        
                    }
                    
                } else {
                    let entity = NSEntityDescription.entity(forEntityName: "CounterCoreData", in: context)
                    let newCounter = NSManagedObject(entity: entity!, insertInto: context)
                    
                    newCounter.setValue(data?.id, forKey: "id")
                    newCounter.setValue(data?.title, forKey: "title")
                    newCounter.setValue(data?.count, forKey: "count")
                }
                
                try context.save()
                
            } catch {
                
                print("Failed saving")
            }
        
        }
        
    }
    
    class func getAllCounter() -> [Counter]? {
        
        var counters: [Counter] = []
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CounterCoreData")
//        request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [CounterCoreData] {
                let counter = Counter()
                counter.id = data.id
                counter.title = data.title
                counter.count = Int(data.count)
                
                counters.append(counter)
            }
            
            return counters
            
        } catch {
            
            print("Failed")
            return nil
        }
        
    }
    
    class func deleteCounter(data: Counter? = nil, id: String? = nil) -> Void {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CounterCoreData")
        request.predicate = NSPredicate(format: "id = %@", data?.id ?? id!)
        request.returnsObjectsAsFaults = false
        
        do {
            
            let result = try context.fetch(request) as! [CounterCoreData]
            for obj in result {
                context.delete(obj)
            }
            
            try context.save()
            
        } catch {
            
            print("Failed")
        }
    }
    
}
