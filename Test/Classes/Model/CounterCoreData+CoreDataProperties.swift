//
//  CounterCoreData+CoreDataProperties.swift
//  Test
//
//  Created by Robert on 15-02-18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//
//

import Foundation
import CoreData


extension CounterCoreData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CounterCoreData> {
        return NSFetchRequest<CounterCoreData>(entityName: "CounterCoreData")
    }

    @NSManaged public var id: String?
    @NSManaged public var title: String?
    @NSManaged public var count: Int16

}
