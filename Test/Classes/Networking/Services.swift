//
//  Services.swift
//  Test
//
//  Created by Robert on 13-02-18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//

import UIKit

let baseURL = "http://localhost:3000/api/v1/"

enum UrlPath: String {
    case getCounter = "counters"
    case addCounter = "counter"
    case incCounter = "counter/inc"
    case decCounter = "counter/dec"
}

enum Method: String {
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
}

var headers: [String: String] = [
    "Content-Type": "application/json"
]

class Services: NSObject {

    class func requestJSON(path: UrlPath, method: Method, params: [String:Any]? = nil, header: [String: Any]? = nil, callback: @escaping (_ data:Data?, _ response: URLResponse?, _ error: NSError?) -> Void) {
        
        let session = URLSession.shared
        
        var request = URLRequest(url: URL(string: "\(baseURL)\(path.rawValue)")!)
        request.httpMethod = method.rawValue
        
        if params != nil {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params!, options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
            }
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            if error != nil {
                callback(nil, response, error! as NSError)
            
            } else {
                callback(data, response, nil)
            }
            
        })
        task.resume()
        
    }
    
}
